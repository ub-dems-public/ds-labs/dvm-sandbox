# Changelog

All notable changes to this project will be documented in this file.

Based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

### Removed


## [v2.1.0/m0] - 2024-09-09

### Added

- matlab
- markdown

### Changed

- maxima
- latex


### Removed


## [v1.1.0/m0] - 2021-09-06

### Added

- julia
- maxima
- stata
- neo4j
- sage
- latex

### Changed


### Removed


## [v1.0.0/m0] - 2019-09-01

### Added

- init project

### Changed


### Removed


