---
title: dvm-sandbox - public git "playground"
subtitle: remote push resricted
author: --
date: 2020-02-12
---


REFERENCES
==========

| *Reference*       | *URL*                                                                          |
|-------------------|--------------------------------------------------------------------------------|
| Online Version    | https://gitlab.com/ub-dems-public/ds-labs/dvm-sandbox/blob/master/README.md    |
| Sandbox Wiki      | https://gitlab.com/ub-dems-public/ds-labs/dvm-sandbox/wikis/home               |



NOTES
=====

GITLAB
------

### Markdown

- [Markdown Guide](https://www.markdownguide.org/)
- [Markdown Tutorial](https://www.markdowntutorial.com/)
- [Mastering Markdown](https://guides.github.com/features/mastering-markdown/)
- [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- [GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)
- [Markdown Style Guide for GitLab](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/)
