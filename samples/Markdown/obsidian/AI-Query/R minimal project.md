## Creating a Valid Internal R Project

### Q:1

For an R developer, considering official CRAN specifications, provide an example of a valid R project, with a minimal mandatory structure, intended only for internal use without CRAN publication, that can be validated by 'R CMD check' (aka "devtools::check()" function of devtools package).

Provide a complete filelist of all project contents, with example files (valid DESCRIPTION, NAMESPACE files, etc.) for a hypothetical package called "Hello" with an exported 'world' function taking two arguments: 'salutation' and 'who', where 'salutation' defaults to the string "Hello". The function returns the formatted string, obtained concatenating 'salutation' argument with string representation of 'who' argument, separated by a single space and with a terminal exclamation mark.

The project should also provide a unit test for this function and a sample R script: 'hello_runner.R', placed under exec/ subdirectory, that invokes the function, passing the name of the operating system user as 'who' argument.


### A:1 (gpt-4)

Below is the minimal file structure for your "Hello" package:

```
Hello/
|--- DESCRIPTION
|--- NAMESPACE
|--- R/
|     |--- world.R
|--- man/
|     |--- world.Rd
|--- tests/
|     |--- testthat.R
|     |--- testthat/
|           |--- test-world.R
|--- exec/
|     |--- hello_runner.R
```
Here are the proposed content for each of these files:

- DESCRIPTION: A meta-data file for the package.

```r
Package: Hello
Type: Package
Title: A Hello World package
Version: 0.1.0
Author: Your Name
Maintainer: Your Name <your.email@somewhere.com>
Description: A minimal package that says hello.
License: What license it uses
Built: 3.6.3
```
- NAMESPACE: Controls the import/export of functions.

```r
export(world)
```

- R/world.R: The implementation of the function.

```r
world <- function(salutation = "Hello", who) {
  paste0(salutation, " ", who, "!")
}
```

- man/world.Rd: The documentation for the function

```r
\name{world}
\alias{world}
\title{World function}
\description{
  A function that says Hello to the world.
}
\usage{
world(salutation = "Hello", who)
}
\arguments{
  \item{salutation}{A string. Default is "Hello".}
  \item{who}{The name of the person to say Hello to.}
}
\examples{
world(salutation = "Hi", who = "John")
}
```

- tests/testthat.R: The setup for the tests.

```r
library(testthat)
test_check("Hello")
```

- tests/testthat/test-world.R: The tests for the world function.

```r
context("Test world function")

test_that("world function works", {
  expect_equal(world(salutation = "Hi", who = "John"), "Hi John!")
})
```

- exec/hello_runner.R: The script that uses the package.

```r
library(Hello)
world(who = Sys.info()["user"])
```
