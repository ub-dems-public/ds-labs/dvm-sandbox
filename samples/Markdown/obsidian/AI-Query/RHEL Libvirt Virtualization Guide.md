``` /// vim: set foldmethod=marker : ```
# ::{{{ #KVM: RHEL setup //
# Q:1 - Setting Up a Virtual RHEL Linux System with Libvirt

You are an expert Linux system administrator specializing in virtualization using libvirt.  Your task is to generate a comprehensive guide for setting up a virtual RHEL Linux system within a RHEL Linux host using libvirt and command-line tools.  The host system is running a standard GNOME desktop environment. Focus on using command-line tools (bash/virsh) whenever possible, minimizing GUI interaction.

The guide should cover the following steps in detail:

1. **Virtualization Software Installation and Configuration:** Provide the commands to install and configure the necessary virtualization packages (e.g., `qemu-kvm`, `libvirt`, `virt-install`) on the RHEL host. Include verification steps to ensure correct installation.

2. **Virtual Machine Setup:**
    * Describe how to create a virtual machine using `virt-install`, specifying the path to a local RHEL distribution ISO file (`{path_to_iso}`).  
    * Explain how to initiate the graphical Anaconda installer for the guest OS setup, but minimize further GUI interaction within the guest.

3. **Network Configuration:**
    * Detail the configuration of a virtual network that allows the guest OS to access the internet through the host.  Use `virsh net-define` and related commands.  Specify an internal network for host/guest communication.
    * Provide instructions for adding a second virtual NIC to the guest VM configuration for direct exposure of guest services to external clients. This second interface should be completely separate from the internal network and configurable within the guest OS.

4. **Virtual Machine Management:**
    * Provide the `virsh` commands to manually start, stop, and check the status of the virtual machine (e.g., `virsh start`, `virsh shutdown`, `virsh dominfo`).

5. **Systemd Integration:**
    * Describe how to configure the host's `systemctl` to automatically start and stop the virtual machine upon host system startup and shutdown.  Include the necessary systemd unit file configuration.

6. **Console Access (No Graphics):**
    * Explain how to connect to the guest machine's system console in a terminal without graphical support. This method should work even during manual boot and allow interaction with the guest's GRUB boot menu (e.g., using `virsh console`).

7. **Remote Graphical Access via SPICE:**
    * Describe how to connect to the virtual machine graphically from a remote computer using the native SPICE protocol. Include necessary configuration steps on both the host and the remote client.

Output the guide in a clear, step-by-step format using Markdown syntax for readability. Use code blocks for commands and configuration snippets.  Provide concise explanations for each step.

# A:1 (Gemini)
> Setting Up a Virtual RHEL Linux System with Libvirt

Okay, here's a comprehensive guide to setting up a virtual RHEL Linux system on a RHEL Linux host using libvirt and command-line tools, along with detailed explanations and example commands.

## Setting Up a Virtual RHEL Linux System with Libvirt

This guide will walk you through setting up a virtual RHEL Linux guest system on a RHEL host using libvirt. The guide emphasizes command-line tools and minimizes GUI interaction.

### 1. Virtualization Software Installation and Configuration

First, we need to install the necessary virtualization packages on the host system.

```bash
# Install the required packages for virtualization.
sudo dnf install -y qemu-kvm libvirt virt-install virt-manager

# Enable and start the libvirtd service.
sudo systemctl enable --now libvirtd

# Verify that libvirt is running correctly.
sudo systemctl status libvirtd

# Check if KVM is loaded correctly (should report KVM acceleration present).
lsmod | grep kvm

# Verify that the 'default' virtual network exists.
virsh net-list --all
```

**Explanation:**
*   We install `qemu-kvm` for virtualization, `libvirt` for managing virtual machines, `virt-install` for creating VMs, and `virt-manager` for GUI management ( although we will mostly avoid using it ).
*   `systemctl enable --now libvirtd` enables the libvirt service and starts it immediately.
*   `systemctl status libvirtd` verifies the service is running.
*   `lsmod | grep kvm` checks if the KVM kernel modules are loaded.
*  `virsh net-list --all` verifies that there is an active network (usually named 'default').

### 2. Virtual Machine Setup

Now, let's create the virtual machine. **Replace `/path/to/your/rhel.iso` with the actual path to your RHEL ISO file.**

```bash
# Create a virtual machine using virt-install.
sudo virt-install \
    --name=rhel-guest \
    --ram=4096 \
    --vcpus=2 \
    --os-variant=rhel9.0 \
    --disk path=/var/lib/libvirt/images/rhel-guest.qcow2,size=20,format=qcow2 \
    --cdrom /path/to/your/rhel.iso \
    --network network=default \
    --graphics vnc,listen=0.0.0.0 \
    --noautoconsole

# Explanation:
# --name: Name of the virtual machine.
# --ram: RAM allocation in MB (4 GB here).
# --vcpus: Number of virtual CPUs.
# --os-variant: Operating system variant for optimal settings.
# --disk: Location and size of the virtual disk (20 GB in qcow2 format).
# --cdrom: Path to the installation ISO file.
# --network: Attach the guest to the 'default' virtual network for internet access.
# --graphics vnc,listen=0.0.0.0 : Opens VNC for connection to the graphical installer of the guest OS, listens on all IPs of the host.
# --noautoconsole: Prevents automatic console redirection, as we'll be using VNC or serial console

```

**Explanation:**
*   This `virt-install` command creates the virtual machine with the specified resources, disk, and network settings. It then launches the graphical installer in a VNC window, accessible from the host.
*   The `--noautoconsole` flag prevents `virt-install` from automatically connecting to the VM's console, allowing us to use VNC instead.
*   You'll need a VNC client on your host or another computer to connect to the VNC server at hostIP:5900

Once the graphical installer starts in the VNC window, proceed with the installation of the guest operating system.

**IMPORTANT:** Minimize any interaction with the GUI installer inside of the VNC session, as this is intended to be a hands-off guide.

### 3. Network Configuration

Next, we'll configure a new virtual network for host-guest communication and add a second NIC for external access.

```bash
# Define a new virtual network for internal host/guest communication.
virsh net-define <<EOF
<network>
  <name>internal-net</name>
  <forward mode='nat'/>
  <ip address='192.168.123.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.123.100' end='192.168.123.254' />
    </dhcp>
  </ip>
</network>
EOF

# Start the newly defined virtual network.
virsh net-start internal-net

# Enable the virtual network for auto-start at libvirt daemon start.
virsh net-autostart internal-net

#Verify that both networks are active.
virsh net-list --all

# Add a second network interface (NIC) to the virtual machine.
virsh attach-device rhel-guest <<EOF
<interface type='network'>
 <source network='internal-net'/>
 <model type='virtio'/>
</interface>
EOF

# Add a second interface (NIC) for direct access via a bridge:
# This creates a bridge between your physical network card (eno1 in this example)
# which allows the guest to access external networks and the local network,
# please replace the bridge name 'br0' and interface 'eno1' if needed.

sudo ip link add name br0 type bridge
sudo ip addr flush dev eno1
sudo ip link set dev eno1 up
sudo ip link set dev br0 up
sudo brctl addif br0 eno1
sudo ip addr add 192.168.1.100/24 dev br0 # change IP to an available address on your network
sudo ip route add default via 192.168.1.1 # change IP to gateway address

# Create a bridge interface for direct access (using the 'br0' bridge) and add it to the guest.

virsh attach-device rhel-guest <<EOF
<interface type='bridge'>
  <source bridge='br0'/>
  <model type='virtio'/>
</interface>
EOF

```

**Explanation:**
*   The `virsh net-define` command defines a new virtual network named `internal-net`. It uses NAT for the guest to connect to the internet through the host's primary interface. It assigns the IP range `192.168.123.0/24`.
*   `virsh net-start` starts the new network, and `virsh net-autostart` ensures it automatically starts when libvirt is started.
*   `virsh attach-device` adds a new virtual network interface to the `rhel-guest` VM, attaching it to the `internal-net` network and uses the virtio model, which is recommended for performance.
*   The `ip link add` command adds a bridge interface, `br0` on the host. The IP address and gateway address must be configured accordingly for the network environment.
*   The second `virsh attach-device` command adds a second NIC to the `rhel-guest` VM, bridging it to the host's `br0`.

### 4. Virtual Machine Management

Now, let's see how to manage the virtual machine.

```bash
# Start the virtual machine.
virsh start rhel-guest

# Get the virtual machine's information.
virsh dominfo rhel-guest

# Shutdown the virtual machine.
virsh shutdown rhel-guest

# Forcefully stop the virtual machine (if shutdown fails).
virsh destroy rhel-guest
```

**Explanation:**
*   `virsh start rhel-guest` starts the VM.
*   `virsh dominfo rhel-guest` displays information about the running VM.
*   `virsh shutdown rhel-guest` gracefully shuts down the VM.
*   `virsh destroy rhel-guest` forcefully stops the VM, which should only be used as last resort.

### 5. Systemd Integration

Let's configure the VM to start and stop with the host system using systemd.

```bash
# Create a systemd unit file for the virtual machine.
sudo tee /etc/systemd/system/libvirt-rhel-guest.service > /dev/null <<EOF
[Unit]
Description=Libvirt virtual machine rhel-guest
After=network.target libvirtd.service
Requires=libvirtd.service

[Service]
Type=oneshot
RemainAfterExit=yes
User=root
Group=root
ExecStart=/usr/bin/virsh start rhel-guest
ExecStop=/usr/bin/virsh shutdown rhel-guest
TimeoutSec=60

[Install]
WantedBy=multi-user.target
EOF

# Enable and start the systemd service.
sudo systemctl enable libvirt-rhel-guest.service
sudo systemctl start libvirt-rhel-guest.service

# Verify that the service is running.
sudo systemctl status libvirt-rhel-guest.service
```

**Explanation:**
*   This creates a systemd unit file that defines how to start and stop the `rhel-guest` VM.
*   `ExecStart` uses `virsh start` to start the VM, and `ExecStop` uses `virsh shutdown`.
*   `systemctl enable` and `systemctl start` enable the service to run at boot and then start the service immediately.
*   `systemctl status` shows that the service is running as expected.

### 6. Console Access (No Graphics)

To connect to the guest console without graphics, use the following command:

```bash
# Connect to the guest's serial console.
virsh console rhel-guest

# Type Ctrl+] to exit the console.
```

**Explanation:**
*   `virsh console rhel-guest` connects to the guest VM's serial console, allowing you to see the GRUB boot menu and interact with the system.
*   Use `Ctrl+]` to exit the console connection.

### 7. Remote Graphical Access via SPICE

To access the guest VM's graphical interface from a remote machine using SPICE, follow these steps:

**On the Host Machine:**

1.  Ensure the guest is configured to use SPICE and that the spice server is listening on the host IP address
        * The `virt-install` command above already configures this with the `--graphics spice,listen=0.0.0.0` flag, alternatively use the virsh command
        `virsh edit rhel-guest` to edit the XML configuration file of the VM to enable spice. Make sure that the following lines exist.
        ```xml
            <graphics type='spice' port='5900' autoport='yes' listen='0.0.0.0'>
              <listen type='address' address='0.0.0.0'/>
            </graphics>
        ```

2. Make sure firewall rules allow traffic to the spice port 5900 on the host system.
        ```bash
        sudo firewall-cmd --permanent --add-port=5900/tcp
        sudo firewall-cmd --reload
        ```

3.  Get the host IP address from the host machine to use for the client connection.
        ```bash
        ip a
        ```

**On the Remote Client Machine:**

1. Install a SPICE client if not already installed. For Fedora/RHEL/CentOS:

    ```bash
    sudo dnf install -y spice-client
    ```

2.  Connect to the guest VM using the client and the IP address of your host machine:
    ```bash
     spicec -h <host_ip_address> -p 5900
    ```

   (replace `<host_ip_address>` with your host IP)

**Explanation:**
*   The spice server is enabled on the guest VM's configuration, and listening on the host IP
*   The spice client will connect to the SPICE server on the guest VM through the defined port.

This should give you a fully functioning virtual RHEL Linux system, manageable from the command line, and accessible remotely through SPICE.





# A:1 (DeepSeek)
> Comprehensive Guide for Setting Up a Virtual RHEL Linux System Using libvirt

This guide provides step-by-step instructions for setting up a virtual
RHEL Linux system on a RHEL Linux host using libvirt and command-line
tools. The focus is on minimizing GUI interaction and leveraging bash
and `virsh` commands.

---

## 1. **Virtualization Software Installation and Configuration**

### Install Required Packages
Install the necessary virtualization packages on the RHEL host:
```bash
sudo dnf install -y qemu-kvm libvirt virt-install virt-viewer virt-manager
```

### Start and Enable libvirt Service
Start the libvirt daemon and enable it to start on boot:
```bash
sudo systemctl start libvirtd
sudo systemctl enable libvirtd
```

### Verify Installation
Ensure the installation was successful:
```bash
sudo virt-host-validate
```
This command checks if the host is capable of running virtual machines. Look for "PASS" messages.

---

## 2. **Virtual Machine Setup**

### Create a Virtual Machine
Use `virt-install` to create a virtual machine with a local RHEL ISO file:
```bash
sudo virt-install \
  --name rhel-vm \
  --ram 2048 \
  --vcpus 2 \
  --disk path=/var/lib/libvirt/images/rhel-vm.qcow2,size=20 \
  --os-type linux \
  --os-variant rhel9.0 \
  --network network=default \
  --graphics spice \
  --cdrom {path_to_iso}
```

Explanation:
- `--name`: Name of the virtual machine.
- `--ram`: Allocated memory in MB.
- `--vcpus`: Number of virtual CPUs.
- `--disk`: Path to the disk image and its size in GB.
- `--os-type` and `--os-variant`: Specify the OS type and variant for optimization.
- `--network`: Attach the VM to the default libvirt network.
- `--graphics`: Use SPICE for graphical output.
- `--cdrom`: Path to the RHEL ISO file.

The graphical Anaconda installer will launch. Complete the installation process with minimal GUI interaction.

---

## 3. **Network Configuration**

### Create an Internal Network
Define and start an internal network for host/guest communication:
```bash
cat <<EOF > internal-network.xml
<network>
  <name>internal</name>
  <bridge name="virbr1"/>
  <ip address="192.168.100.1" netmask="255.255.255.0">
    <dhcp>
      <range start="192.168.100.100" end="192.168.100.200"/>
    </dhcp>
  </ip>
</network>
EOF

sudo virsh net-define internal-network.xml
sudo virsh net-start internal
sudo virsh net-autostart internal
```

### Add a Second Virtual NIC
Add a second NIC to the VM for external communication:
```bash
sudo virsh attach-interface --domain rhel-vm --type network --source default --model virtio --config --live
```

Explanation:
- The first NIC is connected to the internal network.
- The second NIC is connected to the default NAT network for external access.

---

## 4. **Virtual Machine Management**

### Start, Stop, and Check VM Status
Use `virsh` commands to manage the VM:
```bash
# Start the VM
sudo virsh start rhel-vm

# Shut down the VM gracefully
sudo virsh shutdown rhel-vm

# Force stop the VM
sudo virsh destroy rhel-vm

# Check VM status
sudo virsh dominfo rhel-vm
```

---

## 5. **Systemd Integration**

### Create a Systemd Unit File
Create a systemd service to start the VM automatically:
```bash
cat <<EOF | sudo tee /etc/systemd/system/rhel-vm.service
[Unit]
Description=RHEL Virtual Machine
After=network.target libvirtd.service

[Service]
ExecStart=/usr/bin/virsh start rhel-vm
ExecStop=/usr/bin/virsh shutdown rhel-vm
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

# Reload systemd and enable the service
sudo systemctl daemon-reload
sudo systemctl enable rhel-vm.service
```

---

## 6. **Console Access (No Graphics)**

### Connect to the VM Console
Access the VM's console using `virsh console`:
```bash
sudo virsh console rhel-vm
```
To enable console access, ensure the VM's GRUB configuration includes `console=ttyS0` in the kernel command line.

---

## 7. **Remote Graphical Access via SPICE**

### Configure SPICE on the Host
Ensure the VM's XML configuration includes SPICE:
```bash
sudo virsh edit rhel-vm
```
Add or verify the following in the `<devices>` section:
```xml
<graphics type="spice" port="5900" autoport="yes" listen="0.0.0.0">
  <listen type="address" address="0.0.0.0"/>
</graphics>
```

### Connect from a Remote Client
Install a SPICE client on the remote machine (e.g., `virt-viewer`):
```bash
sudo dnf install -y virt-viewer
```
Connect to the VM using:
```bash
remote-viewer spice://{host_ip}:5900
```

---

By following this guide, you can set up and manage a virtual RHEL Linux system on a RHEL host using libvirt and command-line tools. This approach minimizes GUI interaction and ensures efficient virtualization management.

# A:1 (ChatGPT)
> Comprehensive Guide to Setting Up a RHEL Virtual Machine on a RHEL Host Using Libvirt

## 1. Virtualization Software Installation and Configuration

### Install Required Packages
Ensure your RHEL host has the necessary virtualization packages installed:

```bash
sudo dnf install -y qemu-kvm libvirt virt-install virt-viewer
```

### Enable and Start Libvirt Service
Start the `libvirtd` service and enable it to run at boot:

```bash
sudo systemctl enable --now libvirtd
```

### Verify Installation
Check the status of the `libvirtd` service to confirm it is active:

```bash
sudo systemctl status libvirtd
```

Confirm that the KVM module is loaded:

```bash
lsmod | grep kvm
```

Check if your system supports hardware virtualization:

```bash
egrep -c '(vmx|svm)' /proc/cpuinfo
```
If the output is greater than 0, hardware virtualization is supported.

---

## 2. Virtual Machine Setup

### Create a Virtual Machine
Run the following `virt-install` command to create a virtual machine. Replace `{path_to_iso}` with the actual path to your RHEL ISO file:

```bash
sudo virt-install \
  --name rhel-vm \
  --ram 2048 \
  --vcpus 2 \
  --disk path=/var/lib/libvirt/images/rhel-vm.qcow2,size=20 \
  --os-type linux \
  --os-variant rhel8.0 \
  --cdrom {path_to_iso} \
  --graphics spice \
  --network network=default \
  --console pty,target_type=serial
```

### Notes:
- The `--graphics spice` option enables graphical access via SPICE.
- The `--network network=default` connects the VM to the default virtual network.

### Launch the Graphical Installer
The above command will open the graphical Anaconda installer for RHEL in a new window. Complete the OS installation as required.

---

## 3. Network Configuration

### Configure a Virtual Network
To set up an internal virtual network for host-guest communication:

1. Define a new virtual network:

   ```bash
   sudo virsh net-define /tmp/internal-net.xml
   ```

   Example `/tmp/internal-net.xml`:
   ```xml
   <network>
     <name>internal-net</name>
     <forward mode='nat'/>
     <ip address='192.168.100.1' netmask='255.255.255.0'>
       <dhcp>
         <range start='192.168.100.2' end='192.168.100.254'/>
       </dhcp>
     </ip>
   </network>
   ```

2. Start and enable the network:

   ```bash
   sudo virsh net-start internal-net
   sudo virsh net-autostart internal-net
   ```

3. Attach the VM to this network:

   ```bash
   sudo virsh attach-interface --domain rhel-vm --type network --source internal-net --model virtio --config
   ```

### Add a Second NIC for External Access
Attach a second network interface for external access:

```bash
sudo virsh attach-interface --domain rhel-vm --type bridge --source br0 --model virtio --config
```

Replace `br0` with the name of your host’s bridge interface. Configure the guest OS to use this interface as needed.

---

## 4. Virtual Machine Management

### Start the VM
```bash
sudo virsh start rhel-vm
```

### Shut Down the VM
```bash
sudo virsh shutdown rhel-vm
```

### Check the VM Status
```bash
sudo virsh dominfo rhel-vm
```

### List All VMs
```bash
sudo virsh list --all
```

---

## 5. Systemd Integration

### Create a Systemd Unit File
Create a new systemd unit file for the virtual machine:

```bash
sudo nano /etc/systemd/system/rhel-vm.service
```

Example content:

```ini
[Unit]
Description=RHEL Virtual Machine
After=network.target libvirtd.service

[Service]
Type=oneshot
ExecStart=/usr/bin/virsh start rhel-vm
ExecStop=/usr/bin/virsh shutdown rhel-vm
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
```

### Enable the Service
Enable and start the service:

```bash
sudo systemctl enable rhel-vm.service
sudo systemctl start rhel-vm.service
```

---

## 6. Console Access (No Graphics)

### Connect to the Guest Console
Use the `virsh console` command to access the guest's system console:

```bash
sudo virsh console rhel-vm
```

**Explanation:**
*   `virsh console rhel-guest` connects to the guest VM's serial console, allowing you to see the GRUB boot menu and interact with the system.
*   Use `Ctrl+]` to exit the console connection.
---

## 7. Remote Graphical Access via SPICE

### Configure SPICE Access on the Host
Ensure the VM is set up with SPICE graphics by editing its XML configuration:

```bash
sudo virsh edit rhel-vm
```

Add or modify the following section:

```xml
<graphics type='spice' port='5900' autoport='yes' listen='0.0.0.0'>
  <listen type='address' address='0.0.0.0'/>
</graphics>
```

Restart the VM for the changes to take effect:

```bash
sudo virsh shutdown rhel-vm
sudo virsh start rhel-vm
```

### Open Firewall Ports
Allow SPICE connections through the firewall:

```bash
sudo firewall-cmd --add-port=5900/tcp --permanent
sudo firewall-cmd --reload
```

### Connect from a Remote Client
Install a SPICE client (e.g., `virt-viewer`) on the remote machine and connect:

```bash
virt-viewer --connect qemu+ssh://<host_ip>/system rhel-vm
```

Replace `<host_ip>` with the IP address of the RHEL host.

---

This guide provides a step-by-step process for setting up and managing a RHEL virtual machine using libvirt and command-line tools on a RHEL host. Customize configurations as needed for your specific environment.


# ::}}} \\ 1.



# ::{{{ #KVM: RHEL store //
# Q:2 - Libvirt Storage Pool Relocation

Act as an expert Linux system administrator specializing in
virtualization using libvirt. Your task is to relocate the default
libvirt storage pool from its current location at
`/var/lib/libvirt/images` to a new location at
`/home/kvm/libvirt/images`.  The target directory resides on a
separate filesystem and does not yet exist.  A new user `kvm` will own
this directory, and the 'qemu' user (used by the emulator) needs full
access.  Ensure proper SELinux attributes are applied throughout the
process.

Provide a sequence of Linux commands to achieve this relocation, along
with clear explanations for each step.  Consider the following key
aspects:

1. **Verify New Filesystem:** Before proceeding, explain how to verify
   that '/home/kvm/libvirt/images' will reside on a separate
   filesystem. Why is this important for resilience and performance?

2. **Create 'kvm' User and Directory:** Provide commands to create the
   'kvm' user and the directory structure
   '/home/kvm/libvirt/images'. Ensure the directory has the correct
   ownership and permissions for libvirt to function correctly.

3. **Grant 'qemu' User Access:** How would you grant the 'qemu' user
   full access to the new storage pool directory? Explain the chosen
   method and its implications.

4. **Set SELinux Context:** Explain the importance of SELinux and
   provide the command to apply the correct SELinux context to the new
   directory.  Why is `restorecon` used in this scenario?

5. **Relocate Storage Pool:** Detail the steps to relocate the default
   libvirt storage pool to the new location.  Include commands to stop
   relevant services, move the image files, and update libvirt's
   configuration.

6. **Verification:** Finally, explain how to verify that the storage
   pool has been successfully relocated and is accessible by libvirt.
   What commands can be used to confirm the new location and
   permissions?  How would you create a new VM using the relocated
   pool with `virt-install`?


# A:2 (Gemini[gemini-2.0-flash-exp(2)])
> Libvirt Storage Pool Relocation

Okay, let's tackle this libvirt storage pool relocation step-by-step,
with a focus on safety, correctness, and best practices.

**1. Verify New Filesystem**

Before we even touch the data, we need to be absolutely sure that
`/home/kvm/libvirt/images` is on a separate filesystem from the
default `/var/lib/libvirt/images`.  This is critical for resilience.
If the root partition fills up, a virtual machine's disk image might
not be able to write, causing data loss or corruption.  Using a
separate filesystem isolates this risk.  For performance, a different
filesystem can sometimes be on a different physical disk, or a more
performant disk, which can improve the read/write speeds for the guest
images.

To verify the filesystem, we can use the `df` command:

```bash
df -h /var/lib/libvirt/images
df -h /home/kvm/libvirt/images
```
The output will show which device holds each path. For example:

```
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda2        50G   20G   28G  42% /
```
and:
```
Filesystem      Size  Used Avail Use% Mounted on
/dev/sdb1      100G  30G   70G  30% /home/kvm
```
If the output from the two commands have different `/dev/sdX` entries, the paths are on different filesystems. This could also be two partitions on the same physical disk. It is important to understand your underlying setup.

If the new path is on the same partition as the old one, it's critical
to reconsider your placement before proceeding.

**2. Create 'kvm' User and Directory**

Now, let's set up the new home for our images. We'll create the `kvm`
user (as a system user, not an interactive user) and the directory
with proper ownership and basic permissions.

```bash
sudo useradd -r -M -s /bin/false kvm # create user kvm, not for login
sudo mkdir -p /home/kvm/libvirt/images
sudo chown kvm:kvm /home/kvm/libvirt/images
sudo chmod 0700 /home/kvm/libvirt/images #kvm has full access but nobody else can.
```
**Explanation:**

*   `useradd -r -M -s /bin/false kvm`: Creates a system user (`-r`), without a home directory (`-M`), and sets its shell to `/bin/false` (so it cannot log in).
*   `mkdir -p /home/kvm/libvirt/images`: Creates the directory and any parent directories that don't exist.
*   `chown kvm:kvm /home/kvm/libvirt/images`: Sets the owner and group of the directory to `kvm`.
*   `chmod 0700 /home/kvm/libvirt/images`: Gives the owner (kvm) full access and removes any other access. This might be too restrictive later, but provides a starting point.

**3. Grant 'qemu' User Access**

The `qemu` user, which runs the virtual machine processes, needs
access to the storage pool directory. We will accomplish this by
adding the `qemu` user to the `kvm` group.  This is a common, and
effective way to handle file permissions.  This approach avoids using
specific ACLs, which adds complexity.

```bash
sudo usermod -a -G kvm qemu
sudo chmod 0770 /home/kvm/libvirt/images
```
**Explanation:**

*   `usermod -a -G kvm qemu`: Adds the `qemu` user to the `kvm` group (`-a` appends, `-G` specifies groups). Note that this does not make `qemu` the *primary* group, so only members of the `kvm` group will have access to the files, and not other users in `qemu`.
*   `chmod 0770 /home/kvm/libvirt/images`: Changes directory permission to 770, giving both the kvm user, and the kvm group (which `qemu` is now part of) full access. This allows both users to read and write to the directory.

**4. Set SELinux Context**

SELinux (Security-Enhanced Linux) provides mandatory access control,
adding an extra layer of security.  We need to apply the correct
SELinux context to the new directory so that libvirt and QEMU can
operate within it. If SELinux is enabled and an incorrect context is
applied, libvirt will not be able to work correctly.

```bash
sudo chcon -t virt_image_t /home/kvm/libvirt/images
sudo restorecon -v /home/kvm/libvirt/images
```

**Explanation:**

*   `chcon -t virt_image_t /home/kvm/libvirt/images`:  Sets the SELinux type context to `virt_image_t`, which is the correct type for libvirt virtual machine images.
*   `restorecon -v /home/kvm/libvirt/images`:  Applies the SELinux contexts defined by the system policy. The `-v` flag enables verbose output, useful for debugging.  `restorecon` is used because the `chcon` command changes the immediate SELinux context of a file. This is useful for troubleshooting, but on a system reboot, the files will revert to the policy definitions. `restorecon` applies the SELinux policy to the file to make sure it is persistent across system restarts.

**5. Relocate Storage Pool**

Now, we're ready to relocate the storage pool. This involves a few
steps to ensure a safe and clean transition.

```bash
sudo systemctl stop libvirtd
sudo virsh pool-undefault
sudo rsync -av /var/lib/libvirt/images/ /home/kvm/libvirt/images/
sudo virsh pool-define-as default --type dir --target /home/kvm/libvirt/images
sudo virsh pool-start default
sudo systemctl start libvirtd
```
**Explanation:**

*   `systemctl stop libvirtd`: Stops the libvirt daemon to prevent conflicts during the move.
*   `virsh pool-undefault`: Unsets the current default pool, this is necessary for defining a new one.
*   `rsync -av /var/lib/libvirt/images/ /home/kvm/libvirt/images/`:  Copies all image files recursively (`-r`) from the old location to the new location, preserving permissions, timestamps, and symlinks.  The trailing slashes are important, as it copies the *contents* of the old directory into the new one, not the entire directory structure.
*   `virsh pool-define-as default --type dir --target /home/kvm/libvirt/images`: Defines the new default storage pool, pointing to `/home/kvm/libvirt/images` with the type "dir".
*   `virsh pool-start default`: Starts the new default storage pool so it's accessible by libvirt.
*   `systemctl start libvirtd`: Restarts the libvirt daemon to use the new configuration.

**6. Verification**

Finally, we need to verify that everything is working as expected.

```bash
sudo virsh pool-list --all
sudo virsh pool-info default
ls -l /home/kvm/libvirt/images
sudo ls -Z /home/kvm/libvirt/images
```
**Explanation:**

*   `virsh pool-list --all`: Shows the status of all storage pools, including if they are active or inactive, and the name.
*   `virsh pool-info default`:  Provides detailed information about the "default" storage pool, including its path.
*   `ls -l /home/kvm/libvirt/images`: Checks file permissions.
*   `ls -Z /home/kvm/libvirt/images`: Checks SELinux contexts.

**Create New VM:**

To confirm the new location can be used by new VMs, we will use `virt-install`:

```bash
sudo virt-install \
--name test-vm \
--memory 2048 \
--vcpus 2 \
--os-type linux \
--os-variant rhel8.0 \
--disk pool=default,size=10 \
--location 'https://download.fedoraproject.org/pub/fedora/linux/releases/39/Everything/x86_64/os/' \
--graphics none \
--console pty,target_type=serial \
--noautoconsole
```

**Explanation:**

*   `--disk pool=default,size=10`: Specifies that the new virtual disk should be created in the "default" storage pool with a size of 10GB.

**Conclusion**

This comprehensive sequence of steps will relocate your libvirt
storage pool safely, securely, and with proper configuration. Remember
to always back up your data before undertaking significant system
changes. The `rsync` command here provides a very safe transfer, with
error checks and data verification to make sure no data is
corrupted. By double checking the output of the above `virsh` and `ls`
commands, it is possible to verify that no misconfiguration has
occurred.

# A:2 (ChatGPT)
> Relocate libvirt Storage Pool

### Step 1: Verify New Filesystem

To ensure `/home/kvm/libvirt/images` resides on a separate filesystem:

```bash
df -h /home/kvm
```

- **Explanation:** This command displays the mounted filesystems and their mount points. If `/home/kvm` is mounted on a different device (e.g., `/dev/sdX`), it confirms that it is on a separate filesystem. This separation is crucial for resilience (avoiding a single point of failure) and performance (dedicated I/O).

---

### Step 2: Create 'kvm' User and Directory

Create the `kvm` user and set up the directory structure:

```bash
sudo useradd -m kvm
sudo mkdir -p /home/kvm/libvirt/images
sudo chown kvm:kvm /home/kvm/libvirt
sudo chmod 750 /home/kvm/libvirt
sudo chown qemu:qemu /home/kvm/libvirt/images
sudo chmod 770 /home/kvm/libvirt/images
```

- **Explanation:**
  - `useradd -m kvm`: Creates the `kvm` user with a home directory.
  - `mkdir -p`: Creates the necessary directory structure.
  - `chown kvm:kvm /home/kvm/libvirt`: Ensures `kvm` owns the parent directory for flexibility in managing files.
  - `chmod 750`: Restricts access to the parent directory, allowing only the owner (`kvm`) and group access.
  - `chown qemu:qemu` and `chmod 770`: Gives full access to `qemu` for the images directory, as `libvirt` operates under the `qemu` user.

---

### Step 3: Grant 'qemu' User Access

Grant additional access using ACLs for flexibility:

```bash
sudo setfacl -m u:qemu:rwx /home/kvm/libvirt/images
sudo getfacl /home/kvm/libvirt/images
```

- **Explanation:**
  - `setfacl -m u:qemu:rwx`: Explicitly grants the `qemu` user full permissions to the directory.
  - `getfacl`: Verifies the ACLs to ensure they are applied correctly.

---

### Step 4: Set SELinux Context

Adjust SELinux attributes for the new directory:

```bash
sudo semanage fcontext -a -t virt_image_t "/home/kvm/libvirt/images(/.*)?"
sudo restorecon -Rv /home/kvm/libvirt/images
```

- **Explanation:**
  - `semanage fcontext -a`: Associates the `virt_image_t` SELinux context with the directory. This type is required for directories and files used by `libvirt`.
  - `restorecon -Rv`: Recursively applies the correct SELinux context. It ensures SELinux does not block access.

---

### Step 5: Relocate Storage Pool

Relocate the default storage pool to the new directory:

1. Stop the storage pool:
   ```bash
   virsh pool-destroy default
   ```

2. Undefine the storage pool:
   ```bash
   virsh pool-undefine default
   ```

3. Move existing image files:
   ```bash
   sudo mv /var/lib/libvirt/images/* /home/kvm/libvirt/images/
   ```

4. Define the new storage pool:
   ```bash
   virsh pool-define-as --name default --type dir --target /home/kvm/libvirt/images
   ```

5. Start and autostart the storage pool:
   ```bash
   virsh pool-start default
   virsh pool-autostart default
   ```

- **Explanation:**
  - `pool-destroy` and `pool-undefine`: Temporarily disable and remove the old pool configuration.
  - `mv`: Moves existing images to the new location.
  - `pool-define-as`: Defines a new storage pool pointing to the new directory.
  - `pool-start` and `pool-autostart`: Activates the pool and ensures it starts on boot.

---

### Step 6: Verification

To verify the storage pool relocation:

1. Check the pool's status:
   ```bash
   virsh pool-list --all
   ```

2. Check pool details:
   ```bash
   virsh pool-info default
   ```

3. Verify permissions and SELinux context:
   ```bash
   ls -ldZ /home/kvm/libvirt/images
   ```

4. Test with a new VM:
   ```bash
   virt-install \
     --name test-vm \
     --ram 1024 \
     --disk path=/home/kvm/libvirt/images/test-vm.qcow2,size=10 \
     --vcpus 1 \
     --os-variant generic \
     --network network=default \
     --graphics none \
     --console pty,target_type=serial \
     --location 'http://mirrors.kernel.org/centos/8-stream/BaseOS/x86_64/os/' \
     --extra-args 'console=ttyS0,115200n8'
   ```

- **Explanation:**
  - `virt-install`: Creates a test VM using the relocated storage pool.
  - `ls -ldZ`: Confirms SELinux context is correctly set.
  - `pool-list` and `pool-info`: Ensure the pool is defined and active. 

By following these steps, the default libvirt storage pool is safely relocated with appropriate permissions and SELinux context applied.

# A:2 (DeepSeek)
> Relocate libvirt Storage Pool

Relocating the default libvirt storage pool from
`/var/lib/libvirt/images` to `/home/kvm/libvirt/images` involves
several steps, including verifying the new filesystem, creating the
necessary directory structure, setting permissions, applying SELinux
contexts, and updating libvirt's configuration. Below is a detailed
sequence of commands and explanations for each step.

---

### 1. **Verify New Filesystem**
Before proceeding, ensure that `/home/kvm/libvirt/images` resides on a
separate filesystem. This is important for:
- **Resilience:** Isolating VM storage from the root filesystem
  prevents issues with the root filesystem (e.g., running out of
  space) from affecting VM operations.
- **Performance:** A dedicated filesystem can be optimized for VM
  workloads (e.g., using `ext4` or `XFS` with appropriate mount
  options).

To verify the filesystem, use the `df` command:
```bash
df -h /home/kvm
```
If `/home/kvm` is listed as a separate mount point, it confirms that it resides on a separate filesystem. If not, you may need to create and mount a new filesystem.

---

### 2. **Create 'kvm' User and Directory**
Create the `kvm` user and the directory structure with the correct
ownership and permissions.

```bash
# Create the 'kvm' user
sudo useradd -m -d /home/kvm kvm

# Create the directory structure
sudo mkdir -p /home/kvm/libvirt/images

# Set ownership to 'kvm' user and 'kvm' group
sudo chown -R kvm:kvm /home/kvm/libvirt

# Set permissions to allow 'kvm' user and group full access
sudo chmod -R 750 /home/kvm/libvirt
```

Explanation:
- `useradd -m -d /home/kvm kvm`: Creates the `kvm` user with a home directory at `/home/kvm`.
- `mkdir -p`: Creates the directory structure, including parent directories if they don't exist.
- `chown -R kvm:kvm`: Recursively sets ownership of the directory to the `kvm` user and group.
- `chmod -R 750`: Grants the `kvm` user and group full access (read, write, execute) while restricting others.

---

### 3. **Grant 'qemu' User Access**
The `qemu` user (used by the libvirt emulator) needs full access to
the new storage pool directory. This can be achieved by adding the
`qemu` user to the `kvm` group.

```bash
# Add 'qemu' user to the 'kvm' group
sudo usermod -aG kvm qemu

# Verify the group membership
groups qemu
```

Explanation:
- `usermod -aG kvm qemu`: Adds the `qemu` user to the `kvm` group without removing it from other groups.
- The `kvm` group already has full access to the directory due to the `chmod 750` command in the previous step.

---

### 4. **Set SELinux Context**
SELinux enforces mandatory access control, and libvirt requires
specific SELinux contexts for its directories. Use `semanage` and
`restorecon` to apply the correct context.

```bash
# Install SELinux utilities if not already installed
sudo dnf install policycoreutils-python-utils

# Set the default SELinux context for the new directory
sudo semanage fcontext -a -t virt_image_t "/home/kvm/libvirt/images(/.*)?"

# Apply the SELinux context
sudo restorecon -R -v /home/kvm/libvirt/images
```

Explanation:
- `semanage fcontext -a -t virt_image_t`: Adds a rule to set the `virt_image_t` context for the directory and its contents.
- `restorecon -R -v`: Recursively applies the SELinux context based on the rules. This ensures the directory is labeled correctly for libvirt.

---

### 5. **Relocate Storage Pool**
Stop libvirt services, move the image files, and update libvirt's configuration.

```bash
# Stop libvirt services
sudo systemctl stop libvirtd

# Move the image files to the new location
sudo mv /var/lib/libvirt/images/* /home/kvm/libvirt/images/

# Update the default storage pool configuration
sudo virsh pool-edit default
```
In the editor, update the `<path>` element to the new location:
```xml
<path>/home/kvm/libvirt/images</path>
```
Save and exit the editor.

```bash
# Restart libvirt services
sudo systemctl start libvirtd

# Refresh the storage pool
sudo virsh pool-refresh default
```

Explanation:
- `systemctl stop libvirtd`: Stops the libvirt daemon to prevent conflicts during the move.
- `mv`: Moves the VM image files to the new location.
- `virsh pool-edit default`: Edits the XML configuration of the default storage pool to point to the new location.
- `systemctl start libvirtd`: Restarts the libvirt daemon.
- `virsh pool-refresh default`: Refreshes the storage pool to recognize the new location.

---

### 6. **Verification**
Verify that the storage pool has been successfully relocated and is
accessible.

```bash
# Check the storage pool status
sudo virsh pool-info default

# List the contents of the storage pool
sudo virsh vol-list default

# Verify SELinux context
ls -Z /home/kvm/libvirt/images

# Verify permissions
ls -ld /home/kvm/libvirt/images
```

#### Create a New VM Using the Relocated Pool
To confirm the new storage pool is functional, create a new VM using `virt-install`:
```bash
sudo virt-install \
  --name test-vm \
  --ram 2048 \
  --disk path=/home/kvm/libvirt/images/test-vm.qcow2,size=10 \
  --vcpus 2 \
  --os-type linux \
  --os-variant generic \
  --network bridge=virbr0 \
  --graphics none \
  --console pty,target_type=serial \
  --cdrom /path/to/your/iso
```

Explanation:
- `virsh pool-info default`: Displays information about the default storage pool, including its location.
- `virsh vol-list default`: Lists the volumes in the default storage pool.
- `ls -Z`: Displays the SELinux context of the directory.
- `ls -ld`: Displays the directory's permissions and ownership.
- `virt-install`: Creates a new VM using the relocated storage pool.

---

By following these steps, you ensure that the libvirt storage pool is
relocated securely and correctly, with proper permissions and SELinux
contexts applied.

# ::}}} \\ 2.


# ::{{{ #KVM: Network Configuration //
# Q:3 - RHEL 9.5 Libvirt Network Configuration

You are an expert Linux system administrator proficient in RHEL 9.5 and libvirt virtualization. Your task is to design an optimal network configuration for two RHEL 9.5 servers: "op-up310-s41" (host) and "op-up310-s45" (guest). The host will run libvirt, and the guest will be a KVM virtual machine. Focus solely on network planning and configuration, providing bash and virsh commands for implementation. For network configuration try to use only "nmcli" end "virsh" commands 

Requirements:

1. Direct Network Accessibility: Both host and guest must be directly accessible on the physical LAN via the host's physical interface. Use static IP addresses from the 192.168.1.0/24 network: host (192.168.1.105) and guest (192.168.1.115).

2. Host Internet Connectivity: The host has a Wi-Fi adapter with DHCP-assigned internet access.

3. Internal NAT Network: An internal network between host and guest is required. The host will act as a DHCP server for the guest. This network will be used for NAT to provide internet access to the guest, crucial for OS installation and updates.
4.Host network configuration must use main physical network interface configurd as a bridge adapter. Virtualization support must use bridged adpter for intranet access on lan 192.168.1.0. Access through this interface in trusted.
4. Guest HTTP Server Exposure: Provide an example configuration, including firewall rules, to expose an Apache HTTP server running on the guest to the local network (192.168.1.0/24).

5. Guest SSH Access:  The guest must be accessible via SSH from the local network using a pre-existing user account. Specify the necessary configurations.

Deliverables:

6. Network Topology Diagram: A visual representation of the network setup.

7. Host Network Configuration: Bash commands for configuring the host's network interfaces (physical LAN and internal network), including static IP settings, DHCP server setup, firewall rules, and NAT configuration.

8. Guest Network Configuration: Virsh commands and guest OS configuration steps (within the guest OS) for setting up the guest's network interface to obtain an IP address via DHCP from the host and configure firewall rules.

9. Apache HTTP Server Configuration: Example configuration for Apache on the guest and corresponding firewall rules on both guest and host to allow access from the local network.

10. SSH Access Configuration: Instructions for configuring SSH access to the guest from the local network.

11. Verification Steps: Commands to verify the network configuration on both host and guest.

Output Format:  Present your plan in a clear and organized manner, using separate sections for each deliverable. Use code blocks for commands and configuration snippets. Explain each step and its purpose.
Revise markdown syntax, because in default reply, the response starts with a spurious markdown block delimiter.

# A:3 (Gemini)
> RHEL 9.5 Host/Guest Network Configuration Plan
## 1. Network Topology Diagram

```
+-------------------+      Wi-Fi (DHCP)      +-------------------+
|   Host (op-up310-s41)  |----------------------|   Internet         |
|  (RHEL 9.5 + Libvirt)  |                      +-------------------+
+-------+-----------+
        | br0 (192.168.1.105)
        | (Bridge to enp0s3)
        |
        | virbr2 (192.168.100.1)
        | (NAT & DHCP)
        |
+-------+-----------+
|   Guest (op-up310-s45)  |
| (RHEL 9.5 KVM VM) |
| (192.168.1.115 on br0 , 192.168.100.2 via DHCP) |
+-------------------+
```

## 2. Host Network Configuration (op-up310-s41)

Assumptions:

*   `enp0s3`:  Physical Ethernet interface connected to the 192.168.1.0/24 network.
*   `wlp2s0`:  Wi-Fi interface with DHCP-assigned IP address for internet access. (adapt the interface names to your setup!)

```bash
#!/bin/bash

# --- Bridge interface configuration (br0) ---

# Check if the bridge already exists
if nmcli con show br0 &>/dev/null; then
  echo "Bridge 'br0' already exists.  Deleting..."
  nmcli con del br0
fi

# Create the bridge interface
nmcli con add type bridge con-name br0 ifname br0

# Add the ethernet interface to the bridge
nmcli con add type bridge-slave ifname enp0s3 master br0

# Configure the bridge with a static IP
nmcli con mod br0 ipv4.addresses 192.168.1.105/24
nmcli con mod br0 ipv4.gateway 192.168.1.1 # Replace with your actual gateway
nmcli con mod br0 ipv4.dns "192.168.1.1"   # Replace with your actual DNS server(s)
nmcli con mod br0 ipv4.method manual
nmcli con mod br0 connection.autoconnect yes

#Disable the original connection on the physical interface
nmcli con mod enp0s3 connection.autoconnect no

# Activate the connections
nmcli con up br0
nmcli con up enp0s3
# --- NAT network configuration (virbr2) ---
# Define the NAT network name
NAT_NETWORK_NAME="internalnet"

# Check if the NAT network already exists
if virsh net-list --all | grep -q "$NAT_NETWORK_NAME"; then
  echo "NAT network '$NAT_NETWORK_NAME' already exists. Destroying..."
  virsh net-destroy "$NAT_NETWORK_NAME"
  virsh net-undefine "$NAT_NETWORK_NAME"
fi

# Define the NAT network XML
cat <<EOF > nat_network.xml
<network>
  <name>$NAT_NETWORK_NAME</name>
  <forward mode='nat'>
    <nat>
      <port start='1024' end='65535'/>
    </nat>
  </forward>
  <bridge name='virbr2' stp='on' delay='0'/>
  <ip address='192.168.100.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.100.2' end='192.168.100.254'/>
    </dhcp>
  </ip>
</network>
EOF

# Create and start the NAT network
virsh net-define nat_network.xml
virsh net-autostart "$NAT_NETWORK_NAME"
virsh net-start "$NAT_NETWORK_NAME"

rm nat_network.xml

# --- Firewall Configuration (firewalld) ---

# Enable masquerading for the internal network
firewall-cmd --permanent --zone=public --add-masquerade
firewall-cmd --reload

# Allow SSH and HTTP traffic from the local network to the host (if needed)
#firewall-cmd --permanent --zone=public --add-rich-rule='rule family="ipv4" source address="192.168.1.0/24" port protocol="tcp" port="22" accept'
#firewall-cmd --permanent --zone=public --add-rich-rule='rule family="ipv4" source address="192.168.1.0/24" port protocol="tcp" port="80" accept'
#firewall-cmd --reload


echo "Host network configuration complete."
```

**Explanation:**

*   **Bridge (br0):**  Creates a bridge interface `br0`, assigns the static IP `192.168.1.105/24`, gateway, and DNS, and adds the physical Ethernet interface `enp0s3` to the bridge.  The original network connection on `enp0s3` is disabled, and the bridge is brought up. This makes the host accessible on the local network via the `br0` interface.
*   **NAT Network (virbr2):**  Defines a virtual network called `internalnet` using `virsh`.  It configures NAT forwarding, assigns the bridge interface `virbr2` the IP address `192.168.100.1`, and sets up a DHCP server to provide IP addresses in the range `192.168.100.2 - 192.168.100.254` to the guest.  The network is then defined, autostarted, and started.
*   **Firewall (firewalld):**  Enables masquerading on the public zone, which allows traffic from the `192.168.100.0/24` network to be NATed through the host's Wi-Fi interface for internet access. The commented firewall-cmd are if you need ssh or http access to the host.
*   **Interface names:** Adapt the `enp0s3` and `wlp2s0` interface names to reflect the actual names on your system. Use `ip addr` to list network interfaces.

## 3. Guest Network Configuration (op-up310-s45)

**Virsh Configuration (Host)**

```bash
#!/bin/bash

VM_NAME="op-up310-s45"

# Get the current XML definition of the VM
VM_XML=$(virsh dumpxml "$VM_NAME")

# Check if the direct attached network is defined
if [[ "$VM_XML" == *"source bridge='br0'"* ]]; then
    echo "VM already configured with br0.  Skipping."
else

    # Add the XML snippet for the direct attached network
    NETWORK_SNIPPET="
    <interface type='bridge'>
      <mac address='52:54:00:AA:BB:CC'/>
      <source bridge='br0'/>
      <model type='virtio'/>
      <address type='pci' domain='0x0000' bus='0x01' slot='0x01' function='0x0'/>
    </interface>"

    # Insert the network interface definition into the VM XML
    UPDATED_XML=$(echo "$VM_XML" | sed "s#</devices>#$NETWORK_SNIPPET\n</devices>#")

    # Undefine the VM
    virsh undefine "$VM_NAME"

    # Redefine the VM with the updated XML
    echo "$UPDATED_XML" | virsh define /dev/stdin

fi
```

**Guest OS Configuration (op-up310-s45) - Inside the Guest OS**

Edit the network interface configuration file (e.g., `/etc/sysconfig/network-scripts/ifcfg-eth0` or using `nmcli`) to configure the 192.168.1.0/24 access:
```bash
# Example using nmcli
INTERFACE_NAME="eth0" # Adapt this if your network interface is named differently
nmcli con add type ethernet con-name static-eth0 ifname "$INTERFACE_NAME" ip4 192.168.1.115/24 gw4 192.168.1.1 dns4 192.168.1.1
nmcli con mod static-eth0 connection.autoconnect yes
nmcli con up static-eth0

#Example config file /etc/sysconfig/network-scripts/ifcfg-eth0:
#DEVICE=eth0
#ONBOOT=yes
#BOOTPROTO=static
#IPADDR=192.168.1.115
#NETMASK=255.255.255.0
#GATEWAY=192.168.1.1
#DNS1=192.168.1.1
#NAME=eth0
```
**Explanation:**

*   **Host (Virsh):**
    *   The script checks if the VM already has interface to br0, if not, it defines a new network interface in the VM's XML configuration.
    *   `<interface type='bridge'>`: Specifies that the interface will be connected to a bridge.
    *   `<source bridge='br0'/>`: Defines the bridge interface to connect to (br0).
    *   `<model type='virtio'/>`:  Specifies the VirtIO driver for optimal performance.
*   **Guest OS:**
    *  Configures the virtual network interface (eth0) with a static IP address (192.168.1.115/24), the gateway (192.168.1.1) and the DNS server (192.168.1.1).

## 4. Apache HTTP Server Configuration

**Guest OS (op-up310-s45)**

Install Apache:

```bash
sudo dnf install httpd -y # On RHEL 9
sudo systemctl enable httpd
sudo systemctl start httpd
```

Configure Apache (basic example):

Edit `/etc/httpd/conf/httpd.conf` to set `Listen 80`.  The default configuration usually works.

Create a test page:

```bash
echo "<h1>Hello from op-up310-s45!</h1>" | sudo tee /var/www/html/index.html
```

**Guest Firewall:**

```bash
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --reload
```

**Host Firewall:**

```bash
sudo firewall-cmd --permanent --zone=public --add-rich-rule='rule family="ipv4" source address="192.168.1.0/24" destination address="192.168.1.115" port protocol="tcp" port="80" accept'
sudo firewall-cmd --reload
```

**Explanation:**

*   **Guest OS:** Installs, enables, and starts the Apache HTTP server. A simple test page is created.  The guest's firewall is configured to allow HTTP traffic.
*   **Host:** The firewall on the host must allow traffic to the guest. Because we have a 1 to 1 relation with the physical network, the HOST firewall is configured to allow access to the IP Address of the guest.

## 5. SSH Access Configuration

**Prerequisites:**

*   Ensure a user account exists on the guest OS. Let's assume the username is `testuser`.
*   SSH server is installed and running on the guest (usually installed by default on RHEL).

**Guest Firewall:**

```bash
sudo firewall-cmd --permanent --add-service=ssh
sudo firewall-cmd --reload
```

**Host Firewall:**

```bash
sudo firewall-cmd --permanent --zone=public --add-rich-rule='rule family="ipv4" source address="192.168.1.0/24" destination address="192.168.1.115" port protocol="tcp" port="22" accept'
sudo firewall-cmd --reload
```

**Explanation:**

*   The guest's firewall must allow SSH traffic.
*   The host's firewall rule is necessary to forward SSH traffic from the local network (192.168.1.0/24) to the guest's IP address (192.168.1.115) on port 22.

**To connect:**

From a machine on the 192.168.1.0/24 network:

```bash
ssh testuser@192.168.1.115
```

## 6. Verification Steps

**Host (op-up310-s41):**

*   **Network Interfaces:**

```bash
ip addr show br0
ip addr show virbr2
```

Verify that `br0` has the IP `192.168.1.105/24` and `virbr2` has `192.168.100.1`.

*   **Routing Table:**

```bash
route -n
```

Verify there is a route for 192.168.100.0/24 via virbr2.

*   **Firewall:**

```bash
firewall-cmd --list-all --zone=public
```

Verify masquerading is enabled and the SSH/HTTP rules are present.

*   **NAT Check:**

Ping a public address from the host: `ping 8.8.8.8` (from the host's shell prompt, not within a VM). This confirms that the host's internet connectivity is working.

**Guest (op-up310-s45):**

*   **Network Interface:**

```bash
ip addr show eth0
```

Verify that `eth0` has the IP `192.168.1.115/24` and is properly configured.
*   **Gateway:**

```bash
ip route show
```

Verify the default gateway is `192.168.1.1`.
*   **Internet Connectivity (from Guest):**

```bash
ping 8.8.8.8
```

Verify that the guest can ping a public address, confirming NAT is working.

*   **HTTP Access (from another machine on the 192.168.1.0/24 network):**

Open a web browser and navigate to `http://192.168.1.115`. You should see the "Hello from op-up310-s45!" page.

*   **SSH Access (from another machine on the 192.168.1.0/24 network):**

```bash
ssh testuser@192.168.1.115
```
You should be able to connect to the guest via SSH.

This comprehensive plan should provide a solid foundation for setting up the network configuration for your host and guest VMs. Remember to adapt the interface names, IP addresses, and other settings to match your specific environment.



# A:3 (ChatGPT)
> RHEL 9.5 KVM Network Configuration Plan

## **1. Network Topology Diagram**

```
+----------------------+       +----------------------+
|  Physical Network   |       |  Internal NAT Network |
|   (192.168.1.0/24)  |       |   (192.168.2.0/24)   |
|                    |       |                      |
|  +---------------+ |       |  +---------------+  |
|  | Host:         | |       |  | Host:         |  |
|  | 192.168.1.105| |       |  | 192.168.2.1   |  |
|  | (Bridged)     | |       |  | (NAT Gateway) |  |
|  +---------------+ |       |  +---------------+  |
|         |          |       |         |            |
|  +---------------+ |       |  +---------------+  |
|  | Guest:        | |       |  | Guest:        |  |
|  | 192.168.1.115| |       |  | DHCP Assigned |  |
|  | (Bridged)     | |       |  | (192.168.2.100)| |
|  +---------------+ |       |  +---------------+  |
+----------------------+       +----------------------+
```

---

## **2. Host Network Configuration**

### **2.1 Configure the Bridge Interface for LAN**
```bash
nmcli connection add type bridge autoconnect yes con-name br0 ifname br0
nmcli connection modify br0 ipv4.addresses 192.168.1.105/24 ipv4.method manual
nmcli connection modify br0 ipv4.gateway 192.168.1.1
nmcli connection modify br0 ipv4.dns "8.8.8.8,8.8.4.4"
nmcli connection modify br0 bridge.stp no
nmcli connection add type ethernet slave-type bridge con-name br0-slave ifname eth0 master br0
nmcli connection up br0
```

### **2.2 Configure NAT Network for Guest VM**
```bash
nmcli connection add type bridge autoconnect yes con-name br1 ifname br1
nmcli connection modify br1 ipv4.addresses 192.168.2.1/24 ipv4.method manual
nmcli connection modify br1 bridge.stp no
nmcli connection add type ethernet slave-type bridge con-name br1-slave ifname vnet0 master br1
nmcli connection up br1
```

### **2.3 Enable IP Forwarding and Configure NAT**
```bash
echo "net.ipv4.ip_forward = 1" | sudo tee -a /etc/sysctl.conf
sysctl -p
firewall-cmd --permanent --add-masquerade
firewall-cmd --reload
iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
```

### **2.4 Configure DHCP for NAT Network**
```bash
cat << EOF > /etc/dnsmasq.d/virt-dhcp.conf
dhcp-range=192.168.2.100,192.168.2.200,12h
EOF
systemctl restart dnsmasq
```

---

## **3. Guest Network Configuration**

### **3.1 Attach Guest to Networks**
```bash
virsh net-create --file <network-xml-config>
virsh attach-interface --domain op-up310-s45 --type bridge --source br0 --model virtio --config --live
virsh attach-interface --domain op-up310-s45 --type bridge --source br1 --model virtio --config --live
```

### **3.2 Configure Guest to Receive IP via DHCP**
```bash
nmcli connection modify eth1 ipv4.method auto
nmcli connection up eth1
```

---

## **4. Apache HTTP Server Configuration**

### **4.1 Install and Enable Apache on Guest**
```bash
sudo dnf install -y httpd
sudo systemctl enable --now httpd
```

### **4.2 Configure Firewall to Allow HTTP Access**
```bash
firewall-cmd --permanent --add-service=http
firewall-cmd --reload
```

### **4.3 Verify HTTP Access**
```bash
curl http://192.168.1.115
```

---

## **5. SSH Access Configuration**

### **5.1 Enable and Start SSH on Guest**
```bash
sudo systemctl enable --now sshd
```

### **5.2 Configure Firewall to Allow SSH**
```bash
firewall-cmd --permanent --add-service=ssh
firewall-cmd --reload
```

### **5.3 Test SSH Connection**
```bash
ssh user@192.168.1.115
```

---

## **6. Verification Steps**
### **6.1 Check Host Network Configuration**
```bash
nmcli connection show
ip addr show
bridge link show
```

### **6.2 Check Guest Network Configuration**
```bash
ip addr show
nmcli connection show
```

### **6.3 Validate NAT and IP Forwarding**
```bash
iptables -t nat -L -v -n
cat /proc/sys/net/ipv4/ip_forward
```

### **6.4 Verify Connectivity**
```bash
ping -c 4 192.168.1.105
ping -c 4 192.168.1.115
curl http://192.168.1.115
ssh user@192.168.1.115
```

This setup ensures both direct LAN access and an internal NAT network for the guest, with internet access, Apache exposure, and SSH accessibility.







# A:3 (DeepSeek)
> Network Configuration Plan for RHEL 9.5 Host and Guest VM


*Okay, let's tackle this problem step by step. The user wants to set up two RHEL 9.5 servers, one as a host with libvirt and the other as a KVM guest. The main requirements are network configurations using nmcli and virsh. Let's break down the requirements and see how to approach each.*

*First, the host needs a static IP on the physical LAN (192.168.1.105) and the guest should be directly accessible with 192.168.1.115. Also, the host has a Wi-Fi interface with DHCP for internet. There's an internal NAT network between host and guest where the host acts as DHCP server. The host's physical interface must be a bridge for the LAN. The guest needs to run an HTTP server accessible from the LAN and SSH access.*

*Starting with the host's network. The physical interface (say eno1) needs to be bridged. Using nmcli, I'll create a bridge (br0) and add eno1 as a slave. Assign static IP 192.168.1.105/24 to br0. Then, the Wi-Fi interface (wlp2s0) uses DHCP. For the internal NAT, a virtual network (e.g., virbr1) with NAT, DHCP range for the guest. But wait, the guest also needs a direct LAN IP. Hmm, maybe the guest has two interfaces: one bridged to br0 (for LAN access) and one connected to the internal NAT (virbr1) for internet via host's NAT. Wait, but requirement 3 says internal NAT is for internet. But the host's Wi-Fi is for its own internet. So the internal NAT would route through the host's Wi-Fi? Or maybe the internal NAT uses the host's bridge for internet? Need to clarify.*

*Wait, the internal network is for NAT to provide internet to the guest. So the guest's NAT interface uses the host's Wi-Fi for internet. But the host's main internet is via Wi-Fi. So the host's bridge (br0) is on the LAN (192.168.1.0/24) and the Wi-Fi is another interface (maybe wlp2s0) with a different network. Then, the internal NAT network (virbr1) would have NAT using the host's Wi-Fi as the uplink. But how?*

*Alternatively, the internal NAT network (virbr1) uses the host's bridge (br0) as the uplink. But that might not be right. Wait, the host's bridge is for the LAN, which is trusted. The internal NAT network is for the guest's private network. But requirement 3 says the internal network is for NAT to provide internet. So perhaps the guest has two network interfaces: one bridged to br0 (static IP 192.168.1.115) for direct LAN access, and another connected to the internal NAT (virbr1) which uses the host's Wi-Fi as the internet gateway. But how to set that up?*

*Wait, maybe the internal NAT network (virbr1) is separate and uses the host's Wi-Fi for internet. So the host's NAT configuration would route traffic from virbr1 to the Wi-Fi interface. But the host's Wi-Fi is on a different network (like 192.168.0.0/24) via DHCP. Then, the host would perform NAT for virbr1 to the Wi-Fi interface. That way, the guest can use virbr1 for internet access through the host's Wi-Fi. But the guest also has a bridged interface on br0 (192.168.1.115) for direct LAN access.*

*So, the guest would have two interfaces:*

1. *A bridge interface connected to br0 (static IP 192.168.1.115) for LAN access.*

2. *A NAT interface connected to virbr1 (DHCP assigned from the host's internal network) for internet via the host's Wi-Fi.*

*But the user's requirement 1 says both host and guest must be directly accessible on the physical LAN via the host's physical interface. So the guest's bridged interface is on the same LAN as the host's br0. That makes sense. Then, the internal NAT network is a separate network (e.g., 192.168.122.0/24) with the host acting as DHCP server. But how does the host provide internet to the guest through this internal network? The host would need to set up NAT from the internal network to the Wi-Fi interface.*

*Wait, the host's internet is via Wi-Fi (which is DHCP, maybe on a different subnet). The internal NAT network (virbr1) would have NAT to the host's Wi-Fi interface. So when the guest uses the virbr1 interface, it can reach the internet via the host's Wi-Fi. But the guest's main interface is bridged to br0 (LAN 192.168.1.0/24) with static IP. So the guest can have two interfaces: one bridged (for direct LAN access) and one NAT (for internet via host's Wi-Fi). That way, the guest can use the bridged interface for services (HTTP, SSH) on the LAN, and the NAT interface for internet access.*

*But requirement 3 says the internal NAT network is for NAT to provide internet to the guest. So maybe the guest's default route is via the NAT interface. But the bridged interface has a LAN IP. So the guest might need routing configured to use the NAT interface as default gateway. Alternatively, the bridged interface could have a default gateway on the LAN (e.g., 192.168.1.1). But the user might want the guest's internet to go through the host's Wi-Fi. This needs clarification, but the user's requirement says the internal NAT network is for internet access, so the guest must use that interface for internet.*

*So the host's configuration steps:*

1. *Configure the physical interface (eno1) as a bridge (br0) with static IP 192.168.1.105.*

2. *Configure Wi-Fi interface (wlp2s0) with DHCP for host's internet.*

3. *Create a NAT network (virbr1) using virsh, which uses the host's Wi-Fi as the uplink. Wait, but virsh's default NAT network uses the host's default route. If the host's default route is via Wi-Fi, then the NAT network would use that. But need to create a new virtual network for this.*

*Alternatively, create a virtual network with virsh that is NATed to the host's Wi-Fi interface. For example:*

*virsh net-define with forward mode='nat' and the host's Wi-Fi interface as the dev.*

*But maybe the default 'default' network (virbr0) is already set up for NAT. But the user wants a separate internal NAT network. So we can create a new one, say 'intnet', with NAT and DHCP enabled. Then, the host will have iptables rules to NAT traffic from this internal network to the Wi-Fi interface.*

*But perhaps using libvirt's built-in NAT network. Let me check. When you create a virtual network with libvirt, it sets up iptables rules for NAT automatically. So if the host's default route is via Wi-Fi, then the NAT network would route traffic through there.*

*But the host has two interfaces: br0 (192.168.1.105) and wlp2s0 (with DHCP, say 10.0.0.5). The default route is via wlp2s0's gateway. So when the guest uses the internal NAT network (virbr1), the libvirt DHCP will assign an IP (e.g., 192.168.122.2) and the guest's traffic would NAT through the host's wlp2s0 interface.*

*So the guest has two interfaces:*

- *One bridged to br0 (static IP 192.168.1.115)*

- *One connected to the internal NAT network (virbr1) with DHCP.*

*In the guest, the routing table would have two possible default gateways. But to prioritize the NAT interface as default, we can set the metric lower on that interface. Or configure the guest to use the NAT interface as default gateway.*

*But requirement 3 says the internal NAT is for internet access. So during OS installation, the guest can use the NAT interface to get internet. Once installed, the bridged interface can be used for direct LAN access. But the user wants the guest to have both interfaces: bridged for direct LAN access and NAT for internet via host.*

*Now, the host's network configuration steps:*

- *Create bridge br0, assign eno1 to it, set static IP.*

- *Ensure Wi-Fi is connected with DHCP.*

- *Create a NAT network using virsh, which provides DHCP.*

*Host's firewall rules: Need to allow forwarding between interfaces, NAT rules. But libvirt usually handles the NAT rules when creating the network. However, since the host is also bridging, need to make sure the firewall allows traffic on br0 and the NAT interface.*

*For the guest's HTTP server on 192.168.1.115, the host's firewall needs to allow traffic to the guest's IP. But since the guest is on the same LAN via br0, the host's firewall should allow traffic on br0. However, the guest's own firewall (firewalld) needs to allow HTTP and SSH.*

*SSH access to the guest from LAN: The guest has a static IP on the LAN, so SSH should be accessible directly. Just need to configure the guest's firewall to allow SSH.*

*Now, the deliverables:*

4. *Network Topology Diagram: Host has br0 (192.168.1.105) connected to physical LAN (192.168.1.0/24). Guest has two interfaces: bridged to br0 (192.168.1.115) and connected to virbr1 (NAT network, e.g., 192.168.122.0/24). Host's Wi-Fi (wlp2s0) is on another network (DHCP) for internet. Host acts as NAT gateway for virbr1 to Wi-Fi.*

5. *Host Network Configuration: Commands to create br0 with nmcli, assign static IP. Create NAT network with virsh. Firewall rules for NAT and forwarding.*

6. *Guest Configuration: virsh commands to attach two interfaces (bridge and NAT). Guest OS configuration to set static IP on bridged interface, DHCP on NAT. Firewall rules on guest.*

7. *Apache Config: On guest, allow http service, host firewall allows traffic on br0.*

8. *SSH Access: Allow SSH port on guest's firewall.*

9. *Verification: ping, curl, ssh commands.*

*Let me outline the steps.*

*For the host's bridge:*

*nmcli con add type bridge con-name br0 ifname br0 ipv4.method manual ipv4.addresses 192.168.1.105/24 ipv4.gateway 192.168.1.1 ipv4.dns 8.8.8.8*

*nmcli con add type ethernet slave-type bridge con-name br0-slave ifname eno1 master br0*

*nmcli con up br0*

*For the internal NAT network:*

*virsh net-define /path/to/xml*

*But using virsh commands to create a new network:*

*virsh net-create --file internal-net.xml*

*The XML would define a NAT network with DHCP.*

*Alternatively, use virsh net-define with a network named 'intnet' with NAT and DHCP range.*

*Then, when creating the guest VM, attach it to both br0 and the intnet network.*

*But how to attach two interfaces to the guest? The virsh attach-interface command can be used.*

*But when creating the VM, perhaps define two interfaces in the XML.*

*Alternatively, after creating the VM with a single interface (br0), attach another interface to the intnet.*

*But the user wants the guest to have a static IP on br0. So in the guest OS, configure the bridged interface with static IP 192.168.1.115, and the NAT interface via DHCP.*

*Host's DHCP for the internal network: The libvirt network can provide DHCP. So when creating the internal network, specify the DHCP range.*

*Firewall rules on host: Since the bridged network is trusted, maybe no rules needed. For NAT, libvirt handles it. For forwarding between Wi-Fi and internal network, ensure IP forwarding is enabled and proper iptables rules.*

*Host's sysctl net.ipv4.ip_forward=1*

*Host's firewall (firewalld) should allow masquerade on the Wi-Fi interface.*

*For the guest's HTTP server: On guest's firewalld, allow http service. On host, since the guest is on the same LAN, no forwarding needed, but host's firewall should allow traffic on br0 (which it does by default if the zone is trusted).*

*SSH: Similarly, allow ssh on guest's firewall.*

*Example commands:*

*On host, create bridge:*

*nmcli con add type bridge con-name br0 ifname br0 ipv4.addresses 192.168.1.105/24 ipv4.gateway 192.168.1.1 ipv4.dns 8.8.8.8 ipv4.method manual*

*nmcli con add type ethernet slave-type bridge con-name br0-port ifname eno1 master br0*

*nmcli con up br0*

*For the internal NAT network:*

*virsh net-define --file <<EOF*

*<network>*

*<name>intnet</name>*

*<forward mode='nat'/>*

*<bridge name='virbr1' stp='on' delay='0'/>*

*<ip address='192.168.122.1' netmask='255.255.255.0'>*

*<dhcp>*

*<range start='192.168.122.2' end='192.168.122.254'/>*

*</dhcp>*

*</ip>*

*</network>*

*EOF*

*virsh net-start intnet*

*virsh net-autostart intnet*

*Then, when creating the guest VM, attach two interfaces:*

*One interface to bridge br0, another to network intnet.*

*Using virt-install:*

*virt-install --name=op-up310-s45 --ram=2048 --vcpus=2 --disk path=/var/lib/libvirt/images/op-up310-s45.qcow2,size=20 --os-variant rhel9.0 --network bridge=br0 --network network=intnet --cdrom /path/to/rhel9.iso*

*But the user might want to use virsh commands for an existing VM. So after creating the VM, attach the second interface:*

*virsh attach-interface op-up310-s45 network intnet --model virtio --persistent*

*In the guest OS, configure the bridged interface (probably ens3) with static IP:*

*nmcli con mod "Wired connection 1" ipv4.method manual ipv4.addresses 192.168.1.115/24 ipv4.gateway 192.168.1.1 ipv4.dns 8.8.8.8*

*nmcli con up "Wired connection 1"*

*The NAT interface (ens9) would get DHCP from intnet's DHCP server.*

*Firewall on guest:*

*sudo firewall-cmd --permanent --add-service=http*

*sudo firewall-cmd --permanent --add-service=ssh*

*sudo firewall-cmd --reload*

*Host's firewall: Since br0 is in the trusted zone, traffic is allowed.*

*For NAT, libvirt's network handles the rules.*

*To expose the guest's HTTP server, no additional host firewall rules needed because the guest is on the same LAN. But if the host's firewall is blocking, ensure the zone for br0 is trusted.*

*Verification:*

*On host: ping 192.168.1.115, curl http://192.168.1.115*

*On guest: ping 8.8.8.8 (via NAT), check ip a to see both interfaces.*

*SSH from another LAN machine to 192.168.1.115.*

*This should cover the requirements. Now, structure the answer into the required deliverables with commands and explanations.*

Here's the organized implementation plan:

## 0. Network Configuration Plan for RHEL 9.5 Libvirt Host/Guest

```text
[Network Topology Diagram]
Physical LAN (192.168.1.0/24)
├── Host (br0: 192.168.1.105)
│   ├── KVM Guest (br0: 192.168.1.115)
│   └── Libvirt NAT (virbr1: 192.168.122.1/24)
└── Other LAN devices

Host Internet:
└── wlp2s0 (Wi-Fi DHCP)
```

## 1. Host Network Configuration

### Bridge Configuration (Physical LAN)
```bash
# Create bridge for physical interface
sudo nmcli con add type bridge con-name br0 ifname br0 ipv4.method manual \
ipv4.addresses 192.168.1.105/24 ipv4.gateway 192.168.1.1 ipv4.dns 8.8.8.8
sudo nmcli con add type ethernet slave-type bridge con-name br0-slave \
ifname eno1 master br0
sudo nmcli con up br0

# Verify bridge
ip addr show br0
```

### NAT Network Configuration (Internal)
```bash
# Create NAT network for guest internet
sudo virsh net-define /dev/stdin <<EOF
<network>
  <name>intnet</name>
  <forward mode='nat'/>
  <bridge name='virbr1' stp='on' delay='0'/>
  <ip address='192.168.122.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.122.2' end='192.168.122.254'/>
    </dhcp>
  </ip>
</network>
EOF

sudo virsh net-start intnet
sudo virsh net-autostart intnet

# Enable IP forwarding
echo "net.ipv4.ip_forward=1" | sudo tee /etc/sysctl.d/99-ipforward.conf
sudo sysctl -p
```

### Firewall Configuration
```bash
# Configure firewall zones
sudo firewall-cmd --permanent --zone=trusted --add-interface=br0
sudo firewall-cmd --permanent --zone=trusted --add-interface=virbr1
sudo firewall-cmd --permanent --zone=public --add-interface=wlp2s0
sudo firewall-cmd --reload
```

## 2. Guest Network Configuration

### VM Creation with Dual Interfaces
```bash
# Create VM with both networks
sudo virt-install --name op-up310-s45 \
--ram 4096 --vcpus 2 \
--disk path=/var/lib/libvirt/images/op-up310-s45.qcow2,size=20 \
--network bridge=br0 \
--network network=intnet \
--os-variant rhel9.0 \
--cdrom /path/to/rhel9.iso
```

### Guest OS Configuration (Inside VM)
```bash
# Configure bridged interface (static IP)
sudo nmcli con mod "Wired connection 1" \
ipv4.method manual \
ipv4.addresses 192.168.1.115/24 \
ipv4.gateway 192.168.1.1 \
ipv4.dns 8.8.8.8

# Configure NAT interface (DHCP)
sudo nmcli con mod "Wired connection 2" ipv4.method auto

# Apply changes
sudo nmcli con up "Wired connection 1"
sudo nmcli con up "Wired connection 2"
```

## 3. Apache HTTP Server Configuration

### Guest Configuration
```bash
# Install Apache
sudo dnf install httpd -y
sudo systemctl enable --now httpd

# Firewall rules
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --reload

# Create test content
echo "Guest Web Server" | sudo tee /var/www/html/index.html
```

### Host Firewall Rules (If Needed)
```bash
# Only required if using complex routing
sudo firewall-cmd --permanent --zone=trusted --add-rich-rule='rule family="ipv4" source address="192.168.1.0/24" service name="http" accept'
sudo firewall-cmd --reload
```

## 4. SSH Access Configuration

### Guest Configuration
```bash
# Enable SSH access
sudo firewall-cmd --permanent --add-service=ssh
sudo firewall-cmd --reload

# Create user (if needed)
sudo useradd -m sysadmin
sudo passwd sysadmin
```

## 5. Verification Steps

### Host Verification
```bash
# Check bridge connectivity
ping -c 3 192.168.1.115

# Verify NAT network
virsh net-info intnet

# Check firewall rules
sudo firewall-cmd --list-all --zone=trusted
```

### Guest Verification
```bash
# Test internet access
ping -c 3 8.8.8.8

# Check IP assignments
ip -4 addr show

# Verify web server
curl http://192.168.1.115

# Test SSH access (from LAN device)
ssh sysadmin@192.168.1.115
```

This configuration provides:
- Direct LAN access via bridged interface
- NAT-based internet through host's Wi-Fi
- Secure firewall configuration
- Service exposure for web and SSH access

Key security considerations:
- Trusted zone limited to physical LAN interface
- NAT isolation for guest internet traffic
- Minimal firewall rules for required services
- Regular network segmentation between bridge and NAT networks

# ::}}} \\ 3.

