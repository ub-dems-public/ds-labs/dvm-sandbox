
LaTeX and Pandoc integration in Obsidian

* [Obsidian LaTeX Plugins](https://obsidian.md/plugins?search=latex)
* [Mathjax and LaTeX Plugins](https://publish.obsidian.md/hub/02+-+Community+Expansions/02.01+Plugins+by+Category/Mathjax+and+LaTeX+Plugins)
* [Convert academic papers from obsidian to Word/Latex/PDF using pandoc plugin](https://youtu.be/OMPx1dCdcYA?si=8uezB_qNlQbAOC1C)
* [How i write math in Obsidian](https://youtu.be/AaCVP7zqOMU?si=1KD3NJyU_cXc11gw)
