
Tutorials and Quick start video

* [Obsidian Note Taking Tutorial for Beginners - Start HERE (w/ example)](https://youtu.be/d3e7GWsqoU0?si=qWyXggAZWvyjXfIi)
* [Obsidian for Beginners: Start HERE — How to Use the Obsidian App for Notes](https://youtu.be/QgbLb6QCK88?si=-56NB9-tAWQBn7FI)
* [Obsidian As A Second Brain: The ULTIMATE Tutorial](https://youtu.be/WqKluXIra70?si=ecXrzIr8b5gFwHni)
* [Obsidian Quick Start Guide (2024)](https://youtu.be/KWf3tnQkY2Y?si=A4Bs_JdlBZkVs-Cz)
* [You all NEED these Obsidian community plugins](https://youtu.be/Yzi1o-BH6QQ?si=kdk85B4pSK97sJqf)

See [[LaTeX Support]] for LaTeX, Pandoc and [Zotero](https://www.zotero.org/) integration
See [[Demo Vaults]] for note vaults examples
