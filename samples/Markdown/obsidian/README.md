
## Obsidian Intro

 Quote:
 
 > _Obsidian is the private and flexible writing app that adapts to the way you think._

* [Obsidian HomePage](https://obsidian.md/) 
* [Hack your brain with Obsidian.md](https://youtu.be/DbsAQSIKQXk?si=E-zSxgjdNFKIjxu3)


### Obsidian Tutorials

[[Obsidian Tutorials]] note

### LaTeX Support

[[LaTeX Support]] note


### Demo Vaults

[[Demo Vaults]] note


### AI Query Samples

[[AI-query]]
