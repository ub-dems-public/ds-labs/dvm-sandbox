---
title: neo4j tutorials - neo4j intro
subtitle: introduntion to graph db and cypher query language
author: --
date: 2020-06-28
---



REFERENCES
==========

NEO4J Online Tools
------------------


* [NEO4J Sandbox](https://neo4j.com/sandbox)


NEO4J Tutorials
---------------

* [Network and IT Management](https://sandbox.neo4j.com/?usecase=network-management)
* [Network Management Playbook](https://guides.neo4j.com/sandbox/network-management/index.html)




