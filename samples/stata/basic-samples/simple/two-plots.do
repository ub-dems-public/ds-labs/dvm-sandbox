/*
https://stats.idre.ucla.edu/stata/modules/graph8/twoway-scatter-combine/
*/

// sysuse auto.dta, clear

// twoway (scatter mpg weight if foreign ==  0, mcolor(orange)) ///
//           (scatter mpg weight if foreign == 1, mcolor(green)) ///
//           (lfit mpg weight if foreign ==  0, lcolor(orange) lwidth(1.4)) ///
//           (lfit mpg weight if foreign == 1, lcolor(green) lwidth(1.4)), ///
//        legend(label(1 "Domestic") label(2 "Foreign") order(1 2)) ///
//        title("Mileage vs Weight") xtitle("Weight (lbs)") ///
//        ytitle("Mileage")

use https://stats.idre.ucla.edu/stat/stata/notes/hsb2, clear

twoway (scatter read write), by(female)

twoway (scatter read write), by(female ses)

twoway (scatter read write), by(ses female, cols(2))

twoway (scatter read write) ///
       (lfit read write) , /// 
       ytitle(Reading Score)

twoway (scatter read write, mlabel(id)) ///
       (lfit read write, range(30 70)) , ///
       ytitle(Reading Score) by(ses female)

twoway (scatter read write, mlabel(id)) ///
       (lfit read write, range(30 70)) ///
       (lfit read write if id != 51, range(30 70)) if female==1 & ses==3,///
        ytitle(Reading Score) legend(lab(3 "Fitted values without Obs 51"))



